# Pagos Online con Stripe y AdonisJS

Resultado del tutorial: [#006 - Pagos Online con Stripe y AdonisJS](http://www.victorvr.com/tutorial/pagos-online-con-stripe-y-adonisjs)

Demo: [https://adonisjs-stripe.herokuapp.com](https://adonisjs-stripe.herokuapp.com)

![App](/public/app.gif)

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

Credenciales de la [API de Stripe](https://stripe.com/es-mx) `KEY_PUBLISHABLE` y `KEY_SECRET`.

## Instalación de Adonis CLI

Primero necesitamos instalar `Adonis CLI`:

```bash
npm i -g adonisjs-cli
```

## Instalación de Dependencias via NPM

Ahora instalaremos las dependencias de nuestra aplicación:

```bash
npm install
```

## Configurar variables de entorno

Configuramos las siguientes variables de entorno en el archivo `.env`:

```bash
STRIPE_KEY_PUBLISHABLE={YOUR_KEY_PUBLISHABLE}
STRIPE_kEY_SECRET={YOUR_kEY_SECRET}
```

## Iniciar el servidor
Ejecutamos la aplicación:

```bash
adonis serve --dev
```

## Abrir la aplicación
Y por último, abrimos [http://localhost:3333](http://localhost:3333) en el navegador.