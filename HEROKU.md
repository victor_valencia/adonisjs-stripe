# Pagos Online con Stripe y AdonisJS

Resultado del tutorial: [#006 - Pagos Online con Stripe y AdonisJS](http://www.victorvr.com/tutorial/pagos-online-con-stripe-y-adonisjs)

Demo: [https://adonisjs-stripe.herokuapp.com](https://adonisjs-stripe.herokuapp.com)

![App](http://www.victorvr.com/img/posts/Post-06.png)

# Instalación de la aplicación en Heroku

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

## Instalación de Heroku CLI

1.- Primero necesitamos instalar `Heroku CLI`.

```bash
npm install -g heroku
```

2.- Verificamos la instalación `heroku >= 7.0` o mayor.

```bash
heroku --version
```

## Instalación de la aplicación en Heroku

1.- Ingresar al directorio de la aplicación `adonisjs-stripe`.

```bash
cd adonisjs-stripe
```

2.- Iniciar sesión en Heroku con los datos de nuestra cuenta.

```bash
heroku login -i
```

3.- Crea la aplicación `adonisjs-stripe` en Heroku.

```bash
heroku create adonisjs-stripe
```

4.- Agregar el repositorio remoto de Heroku a nuestro repositorio local.

```bash
heroku git:remote -a adonisjs-stripe
```

5.- Agregar las siguientes variables de configuración.

```bash
heroku config:set HOST=::
heroku config:set APP_URL=https://adonisjs-stripe.herokuapp.com
heroku config:set STRIPE_KEY_SECRET={YOUR_STRIPE_KEY_SECRET}
heroku config:set STRIPE_KEY_PUBLISHABLED={YOUR_STRIPE_KEY_PUBLISHABLED}
```

6.- Visualizar las variables de configuración.
```bash
heroku config
```

7.- Implementar nuestra aplicación en Heroku.

```bash
git push heroku master
```

8.- Abrir la aplicación en el navegador.
```bash
heroku open
```