//app/Controllers/Http/ShopController.js
'use strict'

// Recuerde referenciar el controlador StripeController en la parte de arriba
const StripeController = use('App/Controllers/Http/StripeController')
const Stripe = new StripeController()

// Recuerde referenciar la librería Helpers en la parte de arriba
const Helpers = use('Helpers')

// Definimos el objeto "book" como variable global
const book = {
  sku: 'P001',
  title: 'Build Apps with Adonis.JS',
  image: 'http://www.victorvr.com/img/resources/Book-P001.png',
  description: 'Building Node.JS Applications with Javascript.',
  author: 'Victor Valencia Rico',
  price: 5,
  currency: 'USD'
}

class ShopController {

  // Desplegará el producto principal
  async index ({ view, request }) {
    const sessionId = request.input('sessionId')
    return view.render('index', {book: book, sessionId: sessionId} )
  }

  // Método para realizar el pago en la API de Stripe.
  async tryPay({ response }) {
    const success_url = Stripe.getSuccessURL()
    const error_url = Stripe.getErrorURL()
    // Crear el objecto payment
    const payment = {
      payment_method_types: ['card'],
      line_items: [{
        name: book.sku + ' - ' + book.title,
        description: book.description,
        images: [book.image],
        amount: book.price + '00',
        currency: book.currency,
        quantity: 1,
      }],
      success_url: success_url + '?sessionId={CHECKOUT_SESSION_ID}',
      cancel_url: error_url,
    }
    await Stripe.createSession( payment )
      // Indica que la sesión de pago fue creada exitosamente
      .then( ( session ) => {
        return response.redirect('/pay/checkout' + '?sessionId=' + session.id);
      })
      // Indica que la sesión de pago fallida
      .catch( ( err ) => {   
        console.log(err)     
        return response.redirect(error_url + '?name=' + err.type + '&message=' + err.raw.message);
      });
  }

  // Método para realizar el checkout del pago en la API de Stripe.
  async payCheckout ({ view, request }) {
    const sessionId = request.input('sessionId')
    return view.render('checkout', {
      sessionId: sessionId,
      keyPublishable: Stripe.getKeyPublishable()
    })
  }

  // Desplegará la notificación de un pago exitoso
  async paySuccess ({ request, response, session }) {
    const sessionId = request.input('sessionId')
    session.flash({
      sessionId: sessionId,
      notification_class: 'alert-success',
      notification_icon: 'fa-check',
      notification_message: 'Thanks for you purchase! ' + sessionId
    })
    response.redirect('/?sessionId=' + sessionId);
  }

  // Desplegará la notificación de un pago fallido o de otros errores
  async payError ({ request, response, session }) {
    const name = request.input('name')
    const message = request.input('message')
    session.flash({
      notification_class: 'alert-danger',
      notification_icon: 'fa-times-circle',
      notification_message: 'Payment error! ' + name + ': ' + message
    })
    response.redirect('/');
  }

  // Verificará el pago antes de iniciar la descarga
  async download ({ response, request }) {
    const sessionId = request.input('sessionId')
    await Stripe.getSession( sessionId )
      // Indica que el pago existe
      .then( ( session ) => {
        const item = session.display_items[0].custom
        //Descargar Libro
        const name = item.name + '.pdf'
        const source = Helpers.resourcesPath('/files/Book-' + item.name.substr(0, 4) + '.pdf')
        response.attachment(source, name)
      })
      // Indica que el pago no existe
      .catch( ( err ) => {
        //Mostrar Error
        response.send('ERROR: ' + err.type + ' => ' + err.raw.message)
      });
  }

}

module.exports = ShopController