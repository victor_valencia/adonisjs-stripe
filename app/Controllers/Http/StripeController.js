//app/Controllers/Http/StripeController.js
'use strict'

const Config = use('Config')
// Configuramos el Stripe con nuestra clave secreta
const Stripe = use('stripe')(Config.get('stripe.key_secret'))

class StripeController {

  // Returna la clave publicable
  getKeyPublishable () {
    return Config.get('stripe.key_publishable')
  }

  // Returna la URL para procesar un pago exitoso
  getSuccessURL () {
    return Config.get('stripe.url_success')
  }

  // Returna la URL para procesar un pago fallido u otros errores
  getErrorURL () {
    return Config.get('stripe.url_error')
  }

  // Función "Promise" para crear una sesión de pago en la API de Stripe.
  createSession ( payment ) {
    return new Promise( ( resolve, reject ) => {
      Stripe.checkout.sessions.create( payment, function( err, session ) {
        if ( err ) {
          reject(err);
        }
        else {
          resolve(session);
        }
      });
    });
  }

  // Función "Promise" para obtener una sesión de pago en la API de Stripe.
  getSession ( sessionId ) {    
    return new Promise( ( resolve, reject ) => {
      Stripe.checkout.sessions.retrieve( sessionId, function(err, session) {
          if ( err ) {
            reject(err);
          }
          else {            
            resolve(session);              
          }
        }
      );
    });
  }

}

module.exports = StripeController